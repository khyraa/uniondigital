---
title: Projet Big Fernand
subtitle: Site Internet
layout: default
modal-id: 1
date: 2018-03-01
img: bf.png
thumbnail: b-f.jpg
alt: Big Fernand
project-date: Mars 2018
client: Big Fernand
category: Développement Web
description: Après signature du contrat, nous avions pour missions de réaliser un site internet adapté à leur besoin. Nous avions aussi comme but, de travailler le site comme représentatif de la marque Big Fernand au plus proche de son image internationale.

---
